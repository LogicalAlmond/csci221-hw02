#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

void writeFile();

std::string readFile(std::string);

#endif