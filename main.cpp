#include <iostream>
#include <fstream>
#include <string>
#include "encrypt.h"
#include "decrypt.h"
#include "fileOperations.h"
using namespace std;


int main() {
    
    /* 
    Things needed from user
    1: block size (2-8)
        Type: int
        should be checked, N >= 3 && N <= 8
    2: permutation
    3: plaintext file for encrpytion
    4: binary file for decrypting

    Basic steps
    1: get block size and perms from user.
    2: read string from plaintext file
    3: determine if remainder, is so add padding, if not nothing.    
    */

    // Declare size and text
    // initilaize filename
    int size;
    int blocksize = 7;
    string text;
    string fileName = "myfile.txt";

    // Get string from file
    // Get size of the string
    text = readFile(fileName);
    size = text.length();
    int buff = size % blocksize; // sets buff with initial remainder

    while (buff != 0) { //checking previously set remainder
        text += "x";
        size = text.length(); // reassign size
        buff = size % blocksize;   //rechecking remainder
    }

    cout << text << endl;
    cout << size << endl;

    // Initialize char array with size
    char mystr[size];
    stringToArray(mystr, text);
    for(int i = 0; i <= size; i ++) {
        cout << mystr[i] << "";
    }
    cout << endl;

    cipherText(text, blocksize, size);

    return 0;
}