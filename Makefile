permCipher: main.o encrypt.o decrypt.o fileOperations.o
	g++ main.o encrypt.o decrypt.o fileOperations.o -o permCipher

main.o: main.cpp
	g++ -c main.cpp

encrypt.o: encrypt.cpp encrypt.h
	g++ -c encrypt.cpp

decrypt.o: decrypt.cpp decrypt.h
	g++ -c decrypt.cpp

fileOperations.o: fileOperations.cpp fileOperations.h
	g++ -c fileOperations.cpp

clean:
	rm *.o permCipher